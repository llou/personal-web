# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
from typing import Any

sys.path.append(os.curdir)

from pelicanconf import *

SITEURL = 'https://www.llou.net'
RELATIVE_URLS = True

# FEED_ALL_ATOM = "feeds/all.atom.xml" # type: Any
# CATEGORY_FEED_ATOM = "feeds/{slug}.atom.xml" # type: Any

DELETE_OUTPUT_DIRECTORY = True

# Following items are often useful when publishing

#DISQUS_SITENAME = ""
#GOOGLE_ANALYTICS = ""
