#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Jorge Monforte González (llou)'
SITENAME = "llou's website"
SITEURL = ""

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Madrid'

DEFAULT_LANG = 'es'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

PORT = 8001

# Blogroll
LINKS = (('Monforte Technologies', 'https://www.monforte.tech/'),)

# Social widget
SOCIAL = (('twitter', 'https://twitter.com/llou'),
          ('facebook', 'https://facebook.com/jorge.monforte'),)

DEFAULT_PAGINATION = 10

THEME = "theme"

SITEMAP = {
        'format': 'xml',
        'priorities': {
            'articles': 0.6,
            'indexes': 0.5,
            'pages': 0.4
        },
        'changefreqs': {
            'articles': 'weekly',
            'indexes': 'weekly',
            'pages': 'monthly'
                }
}

STATIC_PATHS = ['static', 'images']
ARTICLE_EXCLUDES = ['static']

EXTRA_PATH_METADATA = {
        'static/motos.html': {'path': 'motos.html'},
        }

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True
