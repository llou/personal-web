Title: Master en Ciberseguridad
Date: 1-7-2024
Category: Ciberseguridad
Slug: master-ciberseguridad
Status: published

En el pasado año complete el **Master en Ciberseguridad** de la **Escuela de
Negocios OBS** y la **Universidad de Barcelona**. En este Master buscaba
completar mis conocimientos de informática en lo referente a esta
especialización para poder optar a puestos *DevSecOps* en proyectos
tecnológicos, pero como es lógico y para mi sorpresa, este master estaba mucho
más enfocado a la gestión que la tecnología.

Esto que pudiera verse como un contratiempo se acabó transformado en una
experiencia muy enriquecedora ya que me puse al día en las últimas tendencias
de gestión empresarial (*ITIL 4* y *COBIT 2019*) y como integrar la ciberseguridad
empleándolas y lo mejor fue darme cuenta que los sistemas de gestion de
seguridad en la información (SGSI) se certificaban empleando las normas ISO/UNE
27001, normativa que se alinea perfectamente con sus correspondientes de
*calidad*, *medio ambiente* y *seguridad en el trabajo*, vertientes en las que me
especialice hace años en el master del **Colegio de Ingenieros de Minas del
Noroeste de España**.

Y si a esto le sumamos con el trabajo que estoy haciendo con sistemas de
gestión de contenidos web me ha quedado un curriculum muy adecuado para asistir
en el desarrollo y despliegue de sistemas de gestión, lo que puede suponer otro
campo en el que desarrollarme profesionalmente y aplicar mis conocimientos
tecnológicos.
