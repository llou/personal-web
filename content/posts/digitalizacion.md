Title: La Digitalización
Date: 20-10-2022
Category: Digitalización
Slug: digitalizacion
Lang: es
Status: published
Image: digitalization.jpg

Según yo lo veo la introducción de los ordenadores en nuestras vidas se ha
producido en dos etapas muy diferenciadas una es la fase de la
**informatización** y la otra es la fase de la **digitalización**.

## La Fase de la Informatización

La primera gran revolución de los ordenadores fue la de los PCs, una revolución
liderada por Microsoft que ofrecía una clara estructuración de los sistemas
informáticos en la empresa alrededor de su Directorio Activo, las distintas
máquinas compartían datos y recursos (impresoras). La clave de esta
informatización fué que **las máquinas se adaptaron a los flujos de trabajo
tradicionales en la empresa**.

Con esto quiero decir que el ordenador se convirtió en una máquina de escribir
mejor que la máquina de escribir, en un libro de cuentas mejor que el libro de
cuentas, en un fichero mejor que el fichero, un papel (pdf) mejor que el papel,
la web mejor que la prensa y el correo electrónico sustituyó el correo
convencional,  apareciendo un nuevo personaje, el informático que hacía que
toda aquella infraestructura funcionase, pero sin trastocar las estructuras
organizacionales.

## La Fase de la Digitalización

La cuestión es que este modelo está quedando desfasado y todo principalmente
por la irrupción de una nueva tecnología, el teléfono inteligente que está 
convirtiendo lo que era una herramienta para hacer negocios en el medio en
el que se llevan a cabo estos. La tecnología como servicio está permitiendo
a proyectos empresariales disrumpir modelos tradicionales de negocio, liberando
al empresario de cargas como la gestión logística o la gestión del personal,
  por poner un ejemplo entre los centenares de iniciativas empresariales.

Estos nuevos proyectos, asistidos con tecnologías del espectro de la
inteligencia artificial, altamente conectados mediante nuevas tecnologías como
las cadenas de bloques, requieren de una miriada de profesiones que van mucho
más allá del técnico IT, y que continuamente se van actualizando y
diversificando en una evolución que no parece que vaya a detenerse, al menos
durante unos cuantos años.

**A diferencia con la informatización, con la digitalización es la empresa la
que tiene que buscar adaptarse a los sucesivos avances tecnológicos**, lo que
supone un reto tanto a directivos como profesionales que tienen que estar 
continuamente actualizandose.
