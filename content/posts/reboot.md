Title: ¡Nueva época!
Date: 03-7-2022
Category: meta
Slug: new-age
Lang: es

Bueno, retomo la labor bloguera sin mirar atras con la intención de aportar
contenidos de valor en áreas que no tengan que ver directamente con la extensa
área tecnológica del blog de [Monforte
Technologies](https://www.monforte.tech/blog).
