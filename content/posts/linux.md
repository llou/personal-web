Title: Linux/Unix
Date: 23-10-2022
Category: Digitalización
Slug: linux
Status: draft

Muchas veces te encuentras con la necesidad de correr un programa para controlar
cualquier cosa, sin necesidad de identificación de usuario, sin necesidad de
interfaz gráfico, sin necesidad de controlar el volumen de sonido, simple y
llanamente obtener datos y guardarlos, o mostrarlos mediante una sencilla web.

