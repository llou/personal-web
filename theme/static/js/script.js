    var accionador = document.getElementById("accionador");
    var panel = document.getElementById("panel");
    var visible = false;
    var ancho_panel = 322;

    function actualiza() {
        panel.style.right = visible ? 0 : "-"+ancho_panel+"px";
        accionador.style.background = visible ? "url(aspas.svg)" : "url(hamburguesa.svg)";
    }

    accionador.addEventListener('click', function() {
        visible = !visible;
        actualiza();
    });
